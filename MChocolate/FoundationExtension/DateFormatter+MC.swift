//
//  DateFormatter+MC.swift
//  MChocolate
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

// MARK: - DateFormatter 扩展
extension DateFormatter {
    
    /// 初始化
    ///
    /// - Parameter style: 格式
    convenience init(with style: DateFormatStyle) {
        self.init()
        self.dateFormat = style.rawValue
    }
}
