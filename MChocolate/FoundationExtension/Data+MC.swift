//
//  Data+MC.swift
//  MChocolate
//
//  Created by Richie on 2019/1/4.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

// MARK: - Data Extension
extension Data {
    
    /// 取 Data 中对应的key值 转换成data
    ///
    /// - Parameter key: 要取的key值
    /// - Returns: 返回的data
    public func data(forKey key: String) throws -> Data? {
        var result: Data? = nil
        let object = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableLeaves)
        if let dic = object as? Dictionary<String, Any>, let val = dic[key] {
            result = try JSONSerialization.data(withJSONObject: val, options: .prettyPrinted)
        }
        return result
    }
}
