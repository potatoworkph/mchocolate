//
//  DateStyle.swift
//  MChocolate
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

/// 时间格式
///
/// - date: 日期
/// - datetime: 日期时间
public enum DateStyle {
    
    /// 日期 yyyy-MM-dd
    case date
    
    /// 日期时间 yyyy-MM-dd HH:mm:ss
    case datetime
    
    /// 获取 dateformatter
    public var formatter: DateFormatter {
        get {
            switch self {
            case .date:
                return DateStyle.dateF
            case .datetime:
                return DateStyle.datetimeF
            }
        }
    }
    
    /// 日期
    private static let dateF = DateFormatter(with: .date)

    /// 日期时间
    private static let datetimeF = DateFormatter(with: .datetime)
    
}
