//
//  MoneyStyle.swift
//  MChocolate
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

/// 金额的格式
///
/// - int: Int 0
/// - float: Float 0.00
public enum MoneyStyle {
    /// 整数
    case int
    /// 浮点
    case float
    
    /// 默认值
    public var empty: String {
        get {
            switch self {
            case .int:
                return MoneyStyle.empty_int
            case .float:
                return MoneyStyle.empty_float
            }
        }
    }

    /// 金额的格式
    public var positiveFormat: String {
        get {
            switch self {
            case .int:
                return MoneyStyle.positiveFormat_int
            case .float:
                return MoneyStyle.positiveFormat_float
            }
        }
    }

    /// Int 默认
    fileprivate static let empty_int = "0"

    /// float 默认
    fileprivate static let empty_float = "0.00"

    /// Int 格式
    fileprivate static let positiveFormat_int = "#,##0"

    /// float 格式
    fileprivate static let positiveFormat_float = "#,##0.##"
    
}
