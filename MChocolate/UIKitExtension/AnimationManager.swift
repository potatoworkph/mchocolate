//
//  AnimationManager.swift
//  MChocolate
//
//  Created by Richie on 2019/1/4.
//  Copyright © 2019 person. All rights reserved.
//

import UIKit

/// 动画管理类
public class AnimationManager: NSObject {
    
    /// 单例类
    public static let sharedManager = AnimationManager()
    
    /// 动画 时间间隔 0.3
    public static let duration: TimeInterval = 0.3
    
    /// Modal
    ///
    /// - present: 弹出
    /// - dismiss: 消失
    fileprivate enum ModalStyle {
        case present
        case dismiss
    }
    
    
    /// Modal Animation
    fileprivate class ModalAnimation: NSObject {
        private let style: ModalStyle
        
        /// 初始化方法
        ///
        /// - Parameter style: 类型
        init(with style:ModalStyle) {
            self.style = style
        }
    }
}


// MARK: - UIViewControllerTransitioningDelegate
extension AnimationManager: UIViewControllerTransitioningDelegate {
    /// present
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .present)
    }
    
    /// dismiss
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .dismiss)
    }
}


// MARK: - UIViewControllerAnimatedTransitioning
extension AnimationManager.ModalAnimation: UIViewControllerAnimatedTransitioning {
    ///
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return AnimationManager.duration
    }
    
    ///
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromView = transitionContext.view(forKey: .from), let toView = transitionContext.view(forKey: .to), let to = transitionContext.viewController(forKey: .to), let from = transitionContext.viewController(forKey: .from) else {
            return
        }
        let containerView = transitionContext.containerView
        let finalFrame: CGRect = transitionContext.finalFrame(for: to)
        containerView.addSubview(toView)
        switch self.style {
        case .present:
            toView.frame = finalFrame.offsetBy(dx: finalFrame.width, dy: 0)
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                to.view.frame = finalFrame
            }) { (completeion) in
                transitionContext.completeTransition(true)
            }
        default:
            containerView.addSubview(fromView)
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                from.view.frame = finalFrame.offsetBy(dx: finalFrame.width, dy: 0)
            }) { (completeion) in
                transitionContext.completeTransition(true)
            }
        }
        
    }
}
